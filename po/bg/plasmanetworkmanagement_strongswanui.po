# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-nm package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-nm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-28 00:19+0000\n"
"PO-Revision-Date: 2022-01-02 14:17+0100\n"
"Last-Translator: mkkDr2010 <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: strongswanauth.cpp:59
#, kde-format
msgctxt "@label:textbox password label for private key password"
msgid "Private Key Password:"
msgstr "Парола за частен ключ:"

#: strongswanauth.cpp:61
#, kde-format
msgctxt "@label:textbox password label for smartcard pin"
msgid "PIN:"
msgstr "ПИН:"

#: strongswanauth.cpp:63
#, kde-format
msgctxt "@label:textbox password label for EAP password"
msgid "Password:"
msgstr "Парола:"

#: strongswanauth.cpp:106
#, kde-format
msgctxt "@label:textbox error message while saving configuration"
msgid ""
"Configuration uses ssh-agent for authentication, but no ssh-agent found "
"running."
msgstr ""
"Конфигурацията използва ssh-агент за удостоверяване, но не е намерен работещ "
"ssh-агент."

#. i18n: ectx: property (windowTitle), widget (QWidget, StrongswanAuth)
#. i18n: ectx: property (windowTitle), widget (QWidget, StrongswanProp)
#: strongswanauth.ui:14 strongswanprop.ui:14
#, kde-format
msgid "Strong Swan VPN"
msgstr "Strong Swan VPN"

#. i18n: ectx: property (text), widget (QLabel, passwordLabel)
#: strongswanauth.ui:32
#, kde-format
msgid "Password: "
msgstr "Парола: "

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: strongswanprop.ui:20
#, kde-format
msgid "Gateway"
msgstr "Шлюз"

#. i18n: ectx: property (text), widget (QLabel, textLabel3)
#: strongswanprop.ui:29
#, kde-format
msgid "Gateway:"
msgstr "Шлюз:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#. i18n: ectx: property (text), widget (QLabel, label_4)
#. i18n: ectx: property (text), widget (QLabel, label)
#: strongswanprop.ui:49 strongswanprop.ui:120 strongswanprop.ui:165
#, kde-format
msgid "Certificate:"
msgstr "Сертификат:"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#: strongswanprop.ui:62
#, kde-format
msgid "Authentication"
msgstr "Идентификация"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbMethod)
#: strongswanprop.ui:78
#, kde-format
msgid "Certificate/private key"
msgstr "Сертификат/частен ключ"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbMethod)
#: strongswanprop.ui:83
#, kde-format
msgid "Certificate/ssh-agent"
msgstr "Сертификат/ssh-агент"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbMethod)
#: strongswanprop.ui:88
#, kde-format
msgid "Smartcard"
msgstr "Смарт карта"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbMethod)
#: strongswanprop.ui:93
#, kde-format
msgid "EAP"
msgstr "EAP"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: strongswanprop.ui:130
#, kde-format
msgid "Private key:"
msgstr "Частен ключ:"

#. i18n: ectx: property (text), widget (QLabel, label_7)
#: strongswanprop.ui:140
#, kde-format
msgid "Private Key Password:"
msgstr "Парола за частен ключ:"

#. i18n: ectx: property (text), widget (QLabel, label_8)
#: strongswanprop.ui:182
#, kde-format
msgid "PIN:"
msgstr "ПИН код:"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: strongswanprop.ui:210
#, kde-format
msgid "Username:"
msgstr "Потребителско име:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: strongswanprop.ui:238
#, kde-format
msgid "User Password:"
msgstr "Потребителска парола:"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_3)
#: strongswanprop.ui:255
#, kde-format
msgid "Options"
msgstr "Опции"

#. i18n: ectx: property (text), widget (QCheckBox, innerIP)
#: strongswanprop.ui:264
#, kde-format
msgid "Request an inner IP address"
msgstr "Заявка на вътрешен IP адрес"

#. i18n: ectx: property (text), widget (QCheckBox, udpEncap)
#: strongswanprop.ui:277
#, kde-format
msgid "Enforce UDP encapsulation"
msgstr "Прилагане на UDP капсулиране"

#. i18n: ectx: property (text), widget (QCheckBox, ipComp)
#: strongswanprop.ui:284
#, kde-format
msgid "Use IP compression"
msgstr "Използване на IP компресия"

#. i18n: ectx: property (title), widget (QGroupBox, proposal)
#: strongswanprop.ui:294
#, kde-format
msgid "Enable Custom Cipher Proposals"
msgstr "Активиране на предложения за персонализиран шифър"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: strongswanprop.ui:308
#, kde-format
msgid "IKE:"
msgstr "IKE:"

#. i18n: ectx: property (toolTip), widget (QLineEdit, ike)
#: strongswanprop.ui:315
#, kde-format
msgid "A list of proposals for IKE separated by \";\""
msgstr "Списък с предложения за IKE, разделен с \"; \""

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: strongswanprop.ui:322
#, kde-format
msgid "ESP:"
msgstr "ESP:"

#. i18n: ectx: property (toolTip), widget (QLineEdit, esp)
#: strongswanprop.ui:329
#, kde-format
msgid "A list of proposals for ESP separated by \";\""
msgstr "Списък с предложения за ESP, разделен с \";\""
