# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sergiu Bivol <sergiu@cip.md>, 2013, 2014, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-21 02:02+0000\n"
"PO-Revision-Date: 2022-02-16 14:58+0000\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.2\n"

#: bluetoothmonitor.cpp:67
#, fuzzy, kde-format
#| msgid "We support 'dun' and 'nap' services only."
msgid "Only 'dun' and 'nap' services are supported."
msgstr "Susținem numai serviciile „dun” și „nap”."

#: connectivitymonitor.cpp:63
#, kde-format
msgid "Network authentication"
msgstr ""

#: connectivitymonitor.cpp:70
#, kde-format
msgid "Log in"
msgstr ""

#: connectivitymonitor.cpp:73
#, kde-format
msgid "You need to log into this network"
msgstr ""

#: connectivitymonitor.cpp:93
#, kde-format
msgid "Limited Connectivity"
msgstr ""

#: connectivitymonitor.cpp:94
#, kde-format
msgid ""
"This device appears to be connected to a network but is unable to reach the "
"internet."
msgstr ""

#: modemmonitor.cpp:182
#, kde-format
msgctxt "Text in GSM PIN/PUK unlock error dialog"
msgid "Error unlocking modem: %1"
msgstr "Eroare la deblocarea modemului: %1"

#: modemmonitor.cpp:183
#, kde-format
msgctxt "Title for GSM PIN/PUK unlock error dialog"
msgid "PIN/PUK unlock error"
msgstr "Eroare de deblocare PIN/PUK"

#: notification.cpp:97
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to ConfigFailedReason"
msgid "The device could not be configured"
msgstr ""

#: notification.cpp:100
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"ConfigUnavailableReason"
msgid "IP configuration was unavailable"
msgstr ""

#: notification.cpp:103
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to ConfigExpiredReason"
msgid "IP configuration expired"
msgstr "Configurarea IP a expirat"

#: notification.cpp:106
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to NoSecretsReason"
msgid "No secrets were provided"
msgstr "Nu au fost furnizate secrete"

#: notification.cpp:109
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"AuthSupplicantDisconnectReason"
msgid "Authorization supplicant disconnected"
msgstr ""

#: notification.cpp:113
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"AuthSupplicantConfigFailedReason"
msgid "Authorization supplicant's configuration failed"
msgstr ""

#: notification.cpp:116
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"AuthSupplicantFailedReason"
msgid "Authorization supplicant failed"
msgstr ""

#: notification.cpp:119
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"AuthSupplicantTimeoutReason"
msgid "Authorization supplicant timed out"
msgstr ""

#: notification.cpp:122
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to PppStartFailedReason"
msgid "PPP failed to start"
msgstr "Pornirea PPP a eșuat"

#: notification.cpp:125
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to PppDisconnectReason"
msgid "PPP disconnected"
msgstr "PPP deconectat"

#: notification.cpp:128
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to PppFailedReason"
msgid "PPP failed"
msgstr "PPP a eșuat"

#: notification.cpp:131
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to DhcpStartFailedReason"
msgid "DHCP failed to start"
msgstr "Pornirea DHCP a eșuat"

#: notification.cpp:134
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to DhcpErrorReason"
msgid "A DHCP error occurred"
msgstr "A intervenit o eroare DHCP"

#: notification.cpp:137
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to DhcpFailedReason"
msgid "DHCP failed "
msgstr "DHCP a eșuat "

#: notification.cpp:140
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"SharedStartFailedReason"
msgid "The shared service failed to start"
msgstr "Pornirea dispozitivului partajat a eșuat"

#: notification.cpp:143
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to SharedFailedReason"
msgid "The shared service failed"
msgstr "Serviciul partajat a eșuat"

#: notification.cpp:146
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"AutoIpStartFailedReason"
msgid "The auto IP service failed to start"
msgstr ""

#: notification.cpp:149
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to AutoIpErrorReason"
msgid "The auto IP service reported an error"
msgstr ""

#: notification.cpp:152
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to AutoIpFailedReason"
msgid "The auto IP service failed"
msgstr ""

#: notification.cpp:155
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to ModemBusyReason"
msgid "The modem is busy"
msgstr "Modemul este ocupat"

#: notification.cpp:158
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to ModemNoDialToneReason"
msgid "The modem has no dial tone"
msgstr "Modemul nu are ton de apel"

#: notification.cpp:161
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to ModemNoCarrierReason"
msgid "The modem shows no carrier"
msgstr ""

#: notification.cpp:164
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"ModemDialTimeoutReason"
msgid "The modem dial timed out"
msgstr "Apelarea modemului a expirat"

#: notification.cpp:167
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to ModemDialFailedReason"
msgid "The modem dial failed"
msgstr "Apelarea modemului a eșuat"

#: notification.cpp:170
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to ModemInitFailedReason"
msgid "The modem could not be initialized"
msgstr "Modemul nu a putut fi inițializat"

#: notification.cpp:173
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"GsmApnSelectFailedReason"
msgid "The GSM APN could not be selected"
msgstr "APN-ul GSM nu a putut fi ales"

#: notification.cpp:176
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to GsmNotSearchingReason"
msgid "The GSM modem is not searching"
msgstr "Modemul GSM nu caută"

#: notification.cpp:179
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"GsmRegistrationDeniedReason"
msgid "GSM network registration was denied"
msgstr "Înregistrarea în rețeaua GSM a fost refuzată"

#: notification.cpp:182
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"GsmRegistrationTimeoutReason"
msgid "GSM network registration timed out"
msgstr "Înregistrarea în rețeaua GSM a expirat"

#: notification.cpp:185
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"GsmRegistrationFailedReason"
msgid "GSM registration failed"
msgstr "Înregistrarea GSM a eșuat"

#: notification.cpp:188
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"GsmPinCheckFailedReason"
msgid "The GSM PIN check failed"
msgstr "Verificarea PIN-ului GSM a eșuat"

#: notification.cpp:191
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to FirmwareMissingReason"
msgid "Device firmware is missing"
msgstr "Lipsește microcodul dispozitivului"

#: notification.cpp:194
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to DeviceRemovedReason"
msgid "The device was removed"
msgstr "Dispozitivul a fost detașat"

#: notification.cpp:197
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to SleepingReason"
msgid "The networking system is now sleeping"
msgstr "Sistemul de rețea este adormit acum"

#: notification.cpp:200
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"ConnectionRemovedReason"
msgid "The connection was removed"
msgstr "Conexiunea a fost eliminată"

#: notification.cpp:205
#, kde-format
msgctxt "@info:status Notification when the device failed due to CarrierReason"
msgid "The cable was disconnected"
msgstr "Cablul a fost deconectat"

#: notification.cpp:211
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to ModemNotFoundReason"
msgid "The modem could not be found"
msgstr "Modemul nu poate fi găsit"

#: notification.cpp:214
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to BluetoothFailedReason"
msgid "The bluetooth connection failed or timed out"
msgstr "Conexiunea Bluetooth a eșuat sau a expirat"

#: notification.cpp:217
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to GsmSimNotInserted"
msgid "GSM Modem's SIM Card not inserted"
msgstr "Cartela SIM a modemului GSM nu este introdusă"

#: notification.cpp:220
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to GsmSimPinRequired"
msgid "GSM Modem's SIM Pin required"
msgstr "Este necesar PIN-ul SIM al modemului GSM"

#: notification.cpp:223
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to GsmSimPukRequired"
msgid "GSM Modem's SIM Puk required"
msgstr "Este necesar PUK-ul SIM al modemului GSM"

#: notification.cpp:226
#, kde-format
msgctxt "@info:status Notification when the device failed due to GsmSimWrong"
msgid "GSM Modem's SIM wrong"
msgstr "SIM-ul modemului GSM e greșit"

#: notification.cpp:229
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to InfiniBandMode"
msgid "InfiniBand device does not support connected mode"
msgstr "Dispozitivul InfiniBand nu susține regimul conectat"

#: notification.cpp:232
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to DependencyFailed"
msgid "A dependency of the connection failed"
msgstr "A eșuat o dependență a conexiunii"

#: notification.cpp:235
#, kde-format
msgctxt "@info:status Notification when the device failed due to Br2684Failed"
msgid "Problem with the RFC 2684 Ethernet over ADSL bridge"
msgstr ""

#: notification.cpp:238
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"ModemManagerUnavailable"
msgid "ModemManager not running"
msgstr "ModemManager nu rulează"

#: notification.cpp:241
#, kde-format
msgctxt "@info:status Notification when the device failed due to SsidNotFound"
msgid "The WiFi network could not be found"
msgstr "Rețeaua WiFi nu a fost găsită"

#: notification.cpp:245
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"SecondaryConnectionFailed"
msgid "A secondary connection of the base connection failed"
msgstr "O conexiune secundară a conexiunii de bază a eșuat"

#: notification.cpp:248
#, kde-format
msgctxt "@info:status Notification when the device failed due to DcbFcoeFailed"
msgid "DCB or FCoE setup failed"
msgstr ""

#: notification.cpp:251
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to TeamdControlFailed"
msgid "teamd control failed"
msgstr ""

#: notification.cpp:254
#, kde-format
msgctxt "@info:status Notification when the device failed due to ModemFailed"
msgid "Modem failed or no longer available"
msgstr ""

#: notification.cpp:257
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to ModemAvailable"
msgid "Modem now ready and available"
msgstr ""

#: notification.cpp:260
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to SimPinIncorrect"
msgid "The SIM PIN was incorrect"
msgstr ""

#: notification.cpp:263
#, fuzzy, kde-format
#| msgctxt ""
#| "@info:status Notification when the device failed due to "
#| "ConnectionRemovedReason"
#| msgid "The connection was removed"
msgctxt "@info:status Notification when the device failed due to NewActivation"
msgid "A new connection activation was enqueued"
msgstr "Conexiunea a fost eliminată"

#: notification.cpp:266
#, fuzzy, kde-format
#| msgctxt ""
#| "@info:status Notification when the device failed due to "
#| "DeviceRemovedReason"
#| msgid "The device was removed"
msgctxt "@info:status Notification when the device failed due to ParentChanged"
msgid "The device's parent changed"
msgstr "Dispozitivul a fost detașat"

#: notification.cpp:269
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to ParentManagedChanged"
msgid "The device parent's management changed"
msgstr ""

#: notification.cpp:345
#, kde-format
msgid "Connection '%1' activated."
msgstr "Conexiune „%1” activată."

#: notification.cpp:372
#, kde-format
msgid "Connection '%1' deactivated."
msgstr "Conexiune „%1” dezactivată."

#: notification.cpp:423
#, kde-format
msgid "VPN connection '%1' activated."
msgstr "Conexiune VPN „%1” activată."

#: notification.cpp:426
#, fuzzy, kde-format
#| msgid "VPN connection '%1' activated."
msgid "VPN connection '%1' failed to activate."
msgstr "Conexiune VPN „%1” activată."

#: notification.cpp:429 notification.cpp:437
#, fuzzy, kde-format
#| msgid "VPN connection '%1' activated."
msgid "VPN connection '%1' deactivated."
msgstr "Conexiune VPN „%1” activată."

#: notification.cpp:440
#, fuzzy, kde-format
#| msgid ""
#| "The VPN connection changed state because the device it was using was "
#| "disconnected."
msgid ""
"VPN connection '%1' was deactivated because the device it was using was "
"disconnected."
msgstr ""
"Conexiunea VPN și-a schimbat starea pentru că dispozitivul utilizat de "
"aceasta a fost deconectat."

#: notification.cpp:443
#, fuzzy, kde-format
#| msgid "The service providing the VPN connection was stopped."
msgid "The service providing the VPN connection '%1' was stopped."
msgstr "Serviciul care furniza conexiunea VPN a fost oprit."

#: notification.cpp:446
#, fuzzy, kde-format
#| msgid "The IP config of the VPN connection was invalid."
msgid "The IP config of the VPN connection '%1', was invalid."
msgstr "Configurarea IP a conexiunii VPN era nevalidă."

#: notification.cpp:449
#, kde-format
msgid "The connection attempt to the VPN service timed out."
msgstr ""

#: notification.cpp:452
#, fuzzy, kde-format
#| msgid "The service providing the VPN connection was stopped."
msgid ""
"A timeout occurred while starting the service providing the VPN connection "
"'%1'."
msgstr "Serviciul care furniza conexiunea VPN a fost oprit."

#: notification.cpp:455
#, fuzzy, kde-format
#| msgid "The service providing the VPN connection was stopped."
msgid "Starting the service providing the VPN connection '%1' failed."
msgstr "Serviciul care furniza conexiunea VPN a fost oprit."

#: notification.cpp:458
#, kde-format
msgid "Necessary secrets for the VPN connection '%1' were not provided."
msgstr ""

#: notification.cpp:461
#, kde-format
msgid "Authentication to the VPN server failed."
msgstr "Autentificarea la serverul VPN a eșuat."

#: notification.cpp:464
#, fuzzy, kde-format
#| msgctxt ""
#| "@info:status Notification when the device failed due to "
#| "ConnectionRemovedReason"
#| msgid "The connection was removed"
msgid "The connection was deleted."
msgstr "Conexiunea a fost eliminată"

#: notification.cpp:550
#, kde-format
msgid "No Network Connection"
msgstr ""

#: notification.cpp:551
#, kde-format
msgid "You are no longer connected to a network."
msgstr ""

#: passworddialog.cpp:54
#, kde-format
msgid "Authenticate %1"
msgstr ""

#: passworddialog.cpp:98
#, fuzzy, kde-format
#| msgid "Please provide a password below"
msgid "Provide the password for the wireless network '%1':"
msgstr "Furnizați o parolă mai jos"

#: passworddialog.cpp:100
#, fuzzy, kde-format
#| msgid "Please provide a password below"
msgid "Provide the password for the connection '%1':"
msgstr "Furnizați o parolă mai jos"

#: passworddialog.cpp:106
#, fuzzy, kde-format
#| msgid "Password dialog"
msgid "%1 password dialog"
msgstr "Dialog de introducere a parolei"

#: passworddialog.cpp:126
#, fuzzy, kde-format
#| msgid "Please provide a password below"
msgid "Provide the secrets for the VPN connection '%1':"
msgstr "Furnizați o parolă mai jos"

#: passworddialog.cpp:127
#, fuzzy, kde-format
#| msgid "VPN secrets (%1)"
msgid "VPN secrets (%1) dialog"
msgstr "Secrete VPN (%1)"

#. i18n: ectx: property (windowTitle), widget (QWidget, PasswordDialog)
#: passworddialog.ui:26
#, kde-format
msgid "Password dialog"
msgstr "Dialog de introducere a parolei"

#. i18n: ectx: property (text), widget (QLabel, labelPass)
#: passworddialog.ui:92
#, kde-format
msgid "Password:"
msgstr "Parolă:"

#: pindialog.cpp:67
#, kde-format
msgid "SIM PUK"
msgstr ""

#: pindialog.cpp:69
#, kde-format
msgid "SIM PUK2"
msgstr ""

#: pindialog.cpp:71
#, kde-format
msgid "Service provider PUK"
msgstr ""

#: pindialog.cpp:73
#, kde-format
msgid "Network PUK"
msgstr "PUK rețea"

#: pindialog.cpp:75
#, kde-format
msgid "Corporate PUK"
msgstr "PUK corporativ"

#: pindialog.cpp:77
#, kde-format
msgid "PH-FSIM PUK"
msgstr ""

#: pindialog.cpp:79
#, kde-format
msgid "Network Subset PUK"
msgstr ""

#: pindialog.cpp:82 pindialog.cpp:114
#, kde-format
msgid "%1 unlock required"
msgstr "Este necesară deblocarea %1"

#: pindialog.cpp:83 pindialog.cpp:115
#, kde-format
msgid "%1 Unlock Required"
msgstr "Este necesară deblocarea %1"

#: pindialog.cpp:84 pindialog.cpp:116
#, fuzzy, kde-format
#| msgid ""
#| "The mobile broadband device '%1' requires a SIM PUK code before it can be "
#| "used."
msgid ""
"The mobile broadband device '%1' requires a %2 code before it can be used."
msgstr ""
"Dispozitivul mobil de bandă largă „%1” necesită deblocarea SIM PUK înainte "
"să poată fi folosit."

#: pindialog.cpp:85 pindialog.cpp:117
#, kde-format
msgid "%1 code:"
msgstr "Cod %1:"

#. i18n: ectx: property (text), widget (QLabel, pinLabel)
#: pindialog.cpp:86 pinwidget.ui:153
#, kde-format
msgid "New PIN code:"
msgstr "Cod PIN nou:"

#: pindialog.cpp:87
#, kde-format
msgid "Re-enter new PIN code:"
msgstr "Re-introduceți noul cod PIN:"

#. i18n: ectx: property (text), widget (QCheckBox, chkShowPass)
#: pindialog.cpp:88 pinwidget.ui:193
#, kde-format
msgid "Show PIN/PUK code"
msgstr "Arată codul PIN/PUK"

#: pindialog.cpp:98
#, kde-format
msgid "SIM PIN"
msgstr ""

#: pindialog.cpp:100
#, kde-format
msgid "SIM PIN2"
msgstr ""

#: pindialog.cpp:102
#, kde-format
msgid "Service provider PIN"
msgstr ""

#: pindialog.cpp:104
#, kde-format
msgid "Network PIN"
msgstr "PIN rețea"

#: pindialog.cpp:106
#, kde-format
msgid "PIN"
msgstr "PIN"

#: pindialog.cpp:108
#, kde-format
msgid "Corporate PIN"
msgstr "PIN corporativ"

#: pindialog.cpp:110
#, kde-format
msgid "PH-FSIM PIN"
msgstr ""

#: pindialog.cpp:112
#, kde-format
msgid "Network Subset PIN"
msgstr ""

#: pindialog.cpp:118
#, kde-format
msgid "Show PIN code"
msgstr "Arată codul PIN"

#: pindialog.cpp:200
#, kde-format
msgid "PIN code too short. It should be at least 4 digits."
msgstr "Codul PIN este prea scurt. Ar trebui să conțină cel puțin 4 cifre."

#: pindialog.cpp:205
#, kde-format
msgid "The two PIN codes do not match"
msgstr "Codurile PIN nu se potrivesc"

#: pindialog.cpp:210
#, kde-format
msgid "PUK code too short. It should be 8 digits."
msgstr "Codul PUK este prea scurt. Ar trebui să conțină 8 cifre."

#: pindialog.cpp:215
#, kde-format
msgid "Unknown Error"
msgstr "Eroare necunoscută"

#. i18n: ectx: property (windowTitle), widget (QWidget, PinWidget)
#. i18n: ectx: property (text), widget (QLabel, title)
#: pinwidget.ui:14 pinwidget.ui:47
#, kde-format
msgid "SIM PIN unlock required"
msgstr "Este necesară deblocarea SIM PIN"

#. i18n: ectx: property (text), widget (QLabel, prompt)
#: pinwidget.ui:76
#, no-c-format, kde-format
msgid ""
"The mobile broadband device '%1' requires a SIM PIN code before it can be "
"used."
msgstr ""
"Dispozitivul mobil de bandă largă „%1” necesită deblocarea SIM PIN înainte "
"să poată fi folosit."

#. i18n: ectx: property (text), widget (QLabel, pukLabel)
#: pinwidget.ui:133
#, fuzzy, kde-format
#| msgid "PUK code:"
msgid "PUK code:"
msgstr "Cod PUK:"

#. i18n: ectx: property (text), widget (QLabel, pin2Label)
#: pinwidget.ui:173
#, kde-format
msgid "Re-enter PIN code:"
msgstr "Reintroduceți codul PIN:"

#: secretagent.cpp:407
#, fuzzy, kde-format
#| msgid "Authentication to the VPN server failed."
msgid "Authentication to %1 failed. Wrong password?"
msgstr "Autentificarea la serverul VPN a eșuat."

#~ msgid "VPN connection '%1' failed."
#~ msgstr "Conexiunea VPN „%1” a eșuat."

#~ msgid "VPN connection '%1' disconnected."
#~ msgstr "Conexiunea VPN „%1” a fost deconectată."

#~ msgid "The VPN connection changed state because the user disconnected it."
#~ msgstr ""
#~ "Conexiunea VPN și-a schimbat starea pentru că utilizatorul a deconectat-o."

#~ msgid "The connection was deleted from settings."
#~ msgstr "Conexiunea a fost ștearsă din configurări."

#~ msgid "TextLabel"
#~ msgstr "EtichetăTextuală"

#, fuzzy
#~| msgid "&Show password"
#~ msgid "Show password"
#~ msgstr "&Arată parola"

#, fuzzy
#~| msgid "New PIN code:"
#~ msgid "&New PIN code:"
#~ msgstr "Cod PIN nou:"

#, fuzzy
#~| msgid "Could not contact bluetooth manager (BlueZ)."
#~ msgid "Could not contact Bluetooth manager (BlueZ)."
#~ msgstr "Administratorul de Bluetooth (BlueZ) nu a putut fo contactat."

#, fuzzy
#~| msgid "Default bluetooth adapter not found: %1"
#~ msgid "Default Bluetooth adapter not found: %1"
#~ msgstr "Adaptorul implicit de Bluetooth nu a fost găsit: %1"

#~ msgid "Error activating devices's serial port: %1"
#~ msgstr "Eroare la activarea portului serial al dispozitivului: %1"

#, fuzzy
#~| msgid "Device %1 is not the one we want (%2)"
#~ msgid "Device %1 is not the wanted one (%2)"
#~ msgstr "Dispozitivul %1 nu este cel dorit (%2)"

#~ msgid "Device for serial port %1 (%2) not found."
#~ msgstr "Dispozitivul pentru portul serial %1 (%2) nu a fost găsit."

#~ msgctxt "As in 'Unlock cell phone with this pin code'"
#~ msgid "Unlock"
#~ msgstr "Deblochează"

#~ msgid "SIM PUK unlock required"
#~ msgstr "Este necesară deblocarea SIM PUK"

#~ msgid "SIM PUK Unlock Required"
#~ msgstr "Este necesară deblocarea SIM PUK"

#~ msgid ""
#~ "The mobile broadband device '%1' requires a SIM PUK code before it can be "
#~ "used."
#~ msgstr ""
#~ "Dispozitivul mobil de bandă largă „%1” necesită deblocarea SIM PUK "
#~ "înainte să poată fi folosit."

#~ msgid "SIM PIN Unlock Required"
#~ msgstr "Este necesară deblocarea SIM PIN"

#~ msgid "PIN code:"
#~ msgstr "Cod PIN nou:"
